import { schema } from 'nexus';

const { extendType, intArg } = schema;

extendType({
  type: 'Mutation', definition(t) {
    t.field('deleteBooks', {
      type: 'BatchPayload',
      description: '删除书本',
      args: { ids: intArg({ required: true, list: true }) },
      resolve: async (_, args, ctx) => {
        const { ids } = args;
        return ctx.db.book.deleteMany({ where: { id: { in: ids } } });
      }
    });
  }
});