import { schema } from 'nexus';

import { bookUpdateInput } from '.prisma/client';
import { select } from './const';

const { extendType, stringArg, intArg } = schema;

extendType({
  type: 'Mutation',
  definition(t) {
    t.field('updateBook', {
      description: '更改书本',
      type: 'Book',
      args: {
        id: intArg({ required: true }),
        name: stringArg({ required: false, description: '书名' }),
        description: stringArg({ required: false, description: '简介' }),
        author: stringArg({ required: false, description: '作者' }),
        type: stringArg({ required: false, description: '类型' })
      },
      resolve: async (_, args, ctx) => {
        const { id, name, description, author, type } = args;

        const book = await ctx.db.book.findOne({ where: { id } });
        if (!book) {
          throw Error(`id为${id}的书不存在`);
        }

        const data: bookUpdateInput = {};

        if (name) {
          data.name = name;
        }

        if (description) {
          data.description = description;
        }

        if (author) {
          data.author = author;
        }

        if (type) {
          const types = await ctx.db.book_type.findMany({ where: { name: { equals: type } } });
          if (types.length <= 0) {
            data.book_type = {
              create: { name: type }
            };
          } else {
            data.book_type = { connect: { id: types[0].id } };
          }
        }

        return ctx.db.book.update({ where: { id }, data, select });
      }
    });
  }
});