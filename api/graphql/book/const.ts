import { bookSelect } from '.prisma/client';

export const select: bookSelect = {
  author: true,
  created_at: true,
  description: true,
  type_id: true,
  updated_at: true,
  id: true,
  name: true,
  book_type: true
};