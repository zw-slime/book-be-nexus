import { schema } from 'nexus';
import { select } from './const';

const { extendType, intArg } = schema;

extendType({
  type: 'Query',
  definition(t) {
    t.field('book', {
      type: 'Book',
      args: {
        id: intArg({ required: true })
      },
      description: '获取书本详情',
      resolve: (_, args, ctx) => {
        const { id } = args;
        return ctx.db.book.findOne({
          where: { id }, select
        });
      }
    });
  }
});