import { schema } from 'nexus';
import { select } from './const';

const { extendType, inputObjectType, arg } = schema;

inputObjectType({
  name: 'BooksInput',
  definition(t) {
    t.string('key', { required: true, description: '书名或者简介或者作者关键字搜索', default: '' });
    t.dateTime('updatedAt', { required: true, description: '书本更新时间段', default: [], list: true });
    t.string('type', { required: true, description: '书本类型', default: '' });
  }
});

extendType({
  type: 'Query',
  definition(t) {
    t.field('books', {
      type: 'BookWithPageInfo',
      description: '获取书包列表',
      args: {
        pageInfo: arg({ type: 'PageInfoInput', required: true, default: { pn: 1, ps: 10 } }),
        query: arg({ type: 'BooksInput', required: true })
      },
      resolve: async (_, args, ctx) => {
        const { pageInfo: { pn, ps }, query: { key, updatedAt, type } } = args;
        const data = await ctx.db.book.findMany({
          where: {
            OR: [
              { name: { contains: key } },
              { description: { contains: key } },
              { author: { contains: key } }
            ],
            book_type: {
              name: { contains: type }
            },
            updated_at: {
              gte: updatedAt[0],
              lte: updatedAt[1]
            }
          },
          orderBy: { updated_at: 'asc' },
          first: ps,
          skip: ps * (pn - 1),
          select
        });

        const total = await ctx.db.book.count({
          where: {
            OR: [
              { name: { contains: key } },
              { description: { contains: key } },
              { author: { contains: key } }
            ],
            book_type: {
              name: { contains: type }
            },
            updated_at: {
              gte: updatedAt[0],
              lte: updatedAt[1]
            }
          }
        });
        return { pageInfo: { pn, ps, total }, data };
      }
    });
  }
});