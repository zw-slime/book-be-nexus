import { schema } from 'nexus';
import { select } from './const';

const { extendType, stringArg } = schema;

extendType({
  type: 'Mutation',
  definition(t) {
    t.field('createBook', {
      type: 'Book',
      description: '创建书本',
      args: {
        name: stringArg({ required: true, description: '书名' }),
        description: stringArg({ required: true, description: '书本简介' }),
        type: stringArg({ required: true, description: '类型名称' }),
        author: stringArg({ required: true, description: '作者' })
      },
      resolve: async (_, args, ctx) => {
        const { name, description, type, author } = args;

        const types = await ctx.db.book_type.findMany({ where: { name: type } });

        if (!types || types.length <= 0) {
          return ctx.db.book.create({
            data: {
              name, description, author, book_type: { create: { name: type } }
            }
          });
        } else {
          return ctx.db.book.create({
            data: {
              name, description, author, book_type: { connect: { id: types[0].id } }
            },
            select
          });
        }
      }
    });
  }
});