import { schema } from 'nexus';

schema.objectType({
    name: 'Book',
    definition(t) {
      t.int('id', { nullable: false });
      t.string('name', { description: '书本名称' });
      t.string('author', { description: '作者' });
      t.int('type_id', { description: '书本类型ID' });
      t.string('description', { description: '书本简介' });
      t.dateTime('created_at', { description: '创建时间' });
      t.dateTime('updated_at', { description: '更新时间' });
      t.field('book_type', { type: 'BookType' });
    }
  }
);

schema.objectType({
  name: 'BookWithPageInfo',
  definition(t) {
    t.field('pageInfo', { type: 'PageInfo' });
    t.field('data', { type: 'Book', list: true });
  }
});