import { schema } from 'nexus';

const { extendType, intArg, stringArg } = schema;

extendType({
  type: 'Mutation',
  definition(t) {
    t.field('updateType', {
      type: 'BookType',
      args: {
        id: intArg({ required: true }),
        name: stringArg({ required: true })
      },
      resolve: async (_, args, ctx) => {
        const { id, name } = args;
        const type = await ctx.db.book_type.findOne({ where: { id } });
        if (!type) {
          throw Error(`id为${id}的类型不存在`);
        }
        return ctx.db.book_type.update({ where: { id }, data: { name } });
      }
    });
  }
});