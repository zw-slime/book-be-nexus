import { schema } from 'nexus';

const { extendType, stringArg } = schema;

extendType({
  type: 'Query',
  definition(t) {
    t.list.field('bookTypes', {
      type: 'BookType',
      description: '获取书类型列表',
      args: { name: stringArg({ required: true, default: '' }) },
      resolve: async (_root, args, ctx) => {
        return ctx.db.book_type.findMany({ where: { name: { contains: args.name } } });
      }
    });
  }
});