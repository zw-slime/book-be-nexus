import { schema } from 'nexus';

const { extendType, stringArg } = schema;

extendType({
  type: 'Mutation',
  definition(t) {
    t.field('createBookType', {
      type: 'BookType',
      args: {
        name: stringArg({ required: true, description: '类型名称' })
      },
      resolve: (_, args, ctx) => {
        const { name } = args;
        return ctx.db.book_type.create({ data: { name } });
      }
    });
  }
});