import { schema } from 'nexus';

const { objectType } = schema;

objectType({
  name: 'BookType',
  description: '书本类型列表',
  definition(t) {
    t.int('id');
    t.string('name', { description: '类型名称' });
    t.dateTime('created_at', { description: '创建时间' });
    t.dateTime('updated_at', { description: '更新时间' });
    t.int('book_count', {
      description: '书本总数',
      resolve: async (root, _, ctx) => {
        return ctx.db.book.count({ where: { type_id: root.id } });
      }
    });
  }
});