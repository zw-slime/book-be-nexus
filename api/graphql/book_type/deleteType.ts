import { schema } from 'nexus';

const { extendType, intArg } = schema;

extendType({
  type: 'Mutation',
  definition(t) {
    t.field('deleteBookTypes', {
      type: 'BatchPayload',
      args: { ids: intArg({ required: true, list: true }) },
      resolve: async (_, args, ctx) => {
        const { ids } = args;

        const types = await ctx.db.book_type.findMany({ where: { id: { in: ids } } });
        if (!types || types.length <= 0) {
          throw Error('传入的id不存在');
        }

        await ctx.db.book.deleteMany({ where: { type_id: { in: ids } } });

        return ctx.db.book_type.deleteMany({ where: { id: { in: ids } } });
      }
    });
  }
});