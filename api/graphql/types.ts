import { schema } from 'nexus';
import { Kind } from 'graphql';
import { TaskStateEnum } from './book_section/const';

const { objectType, inputObjectType, scalarType, enumType } = schema;

objectType({
  name: 'PageInfo',
  definition(t) {
    t.int('pn');
    t.int('ps');
    t.int('total');
  }
});

inputObjectType({
  name: 'PageInfoInput',
  definition(t) {
    t.int('pn', { required: true });
    t.int('ps', { required: true });
  }
});

objectType({
  name: 'BatchPayload',
  definition(t) {
    t.int('count');
  }
});

scalarType({
  name: 'DateTime',
  asNexusMethod: 'dateTime',
  description: 'Date custom scalar type',
  parseValue(value) {
    return new Date(value);
  },
  serialize(value) {
    return value.getTime();
  },
  parseLiteral(ast) {
    if (ast.kind === Kind.INT) {
      return new Date(ast.value);
    }
    return null;
  }
});

enumType({
  name: 'TaskStateEnum',
  members: [TaskStateEnum.Waiting, TaskStateEnum.Doing, TaskStateEnum.Success, TaskStateEnum.Error]
});