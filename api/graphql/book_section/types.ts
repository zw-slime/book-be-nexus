import { schema } from 'nexus';

const { objectType } = schema;

objectType({
  name: 'BookSection',
  description: '书本章节',
  definition(t) {
    t.int('id');
    t.string('title', { description: '章节标题' });
    t.string('content', { description: '章节内容' });
    t.int('book_id', { description: '所属书本的Id' });
    t.string('download_url', { description: '章节链接地址' });
    t.dateTime(('created_at'));
    t.dateTime('updated_at');
    t.field('status', { description: '任务状态', type: 'TaskStateEnum' });
  }
});