import { schema } from 'nexus';
import { TaskStateEnum } from './const';

const { extendType, stringArg, intArg, arg } = schema;

extendType({
  type: 'Mutation', definition(t) {
    t.field('createBookSection', {
      type: 'BookSection',
      args: {
        bookId: intArg({ required: true }),
        title: stringArg({ required: true, description: '章节标题' }),
        content: stringArg({ required: false, description: '章节内容', default: '' }),
        downloadUrl: stringArg({ required: true, description: '章节链接地址' }),
        status: arg({ required: false, description: '章节下载状态', default: TaskStateEnum.Waiting, type: 'TaskStateEnum' })
      },
      description: '新增章节',
      resolve: async (_, args, ctx) => {
        const { bookId, title, content, downloadUrl, status } = args;

        const book = await ctx.db.book.findOne({ where: { id: bookId } });
        if (!book) {
          throw Error(`ID为${bookId}的书本不存在`);
        }

        return ctx.db.book_section.create({
          data: {
            title,
            content: content as string,
            download_url: downloadUrl,
            status: status as TaskStateEnum,
            book: {
              connect: { id: bookId }
            }
          }
        });
      }
    });
  }
});