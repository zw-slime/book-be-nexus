import { book_sectionSelect } from '.prisma/client';

export const select: book_sectionSelect = {
  id: true,
  title: true,
  content: true,
  book_id: true,
  download_url: true,
  created_at: true,
  updated_at: true,
  status: true
};

export enum TaskStateEnum {
  Waiting = 'waiting',
  Doing = 'doing',
  Success = 'success',
  Error = 'error',
}