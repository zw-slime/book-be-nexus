import { schema } from 'nexus';
import { select } from './const';

const { extendType, intArg } = schema;

extendType({
  type: 'Query', definition(t) {
    t.list.field('bookSections', {
      type: 'BookSection',
      description: '获取书包章节列表',
      args: { bookId: intArg({ required: true }) },
      resolve: async (_, args, ctx) => {
        const { bookId } = args;
        const book = await ctx.db.book.findOne({ where: { id: bookId } });
        if (!book) {
          throw Error(`ID为${bookId}的书本不存在`);
        }
        return ctx.db.book_section.findMany({ where: { book_id: bookId },select });
      }
    });
  }
});