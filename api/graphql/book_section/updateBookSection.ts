import { schema } from 'nexus';
import { select, TaskStateEnum } from './const';
import { book_sectionUpdateInput } from '.prisma/client';

const { extendType, stringArg, intArg, arg } = schema;

extendType({
  type: 'Mutation', definition(t) {
    t.field('updateBookSection', {
      type: 'BookSection',
      args: {
        id: intArg({ required: true }),
        title: stringArg({ required: false, description: '章节标题' }),
        content: stringArg({ required: false, description: '章节内容' }),
        downloadUrl: stringArg({ required: false, description: '章节链接地址' }),
        status: arg({ required: false, description: '章节下载状态', type: 'TaskStateEnum' })
      },
      description: '修改章节',
      resolve: async (_, args, ctx) => {
        const { id, title, content, downloadUrl, status } = args;

        const book = await ctx.db.book_section.findOne({ where: { id } });
        if (!book) {
          throw Error(`ID为${id}的书本章节不存在`);
        }

        const data: book_sectionUpdateInput = {};
        if (title) {
          data.title = title;
        }
        if (content) {
          data.content = content;
        }
        if (downloadUrl) {
          data.download_url = downloadUrl;
        }
        if (status) {
          data.status = status;
        }
        return ctx.db.book_section.update({
          data, where: { id }, select
        });
      }
    });
  }
});