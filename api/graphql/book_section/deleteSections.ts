import { schema } from 'nexus';

const { extendType, intArg } = schema;

extendType({
  type: 'Mutation',
  definition(t) {
    t.field('deleteSections', {
      type: 'BatchPayload',
      description: '删除章节列表',
      args: { ids: intArg({ required: true, list: true }) },
      resolve: (_, args, ctx) => {
        const { ids } = args;
        return ctx.db.book_section.deleteMany({ where: { id: { in: ids } } });
      }
    });
  }
});