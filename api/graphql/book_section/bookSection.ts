import { schema } from 'nexus';
import { select } from './const';

const { extendType, intArg } = schema;

extendType({
  type: 'Query',
  definition(t) {
    t.field('bookSection', {
      description: '获取章节详情',
      type: 'BookSection',
      args: { id: intArg({ required: true }) },
      resolve: (_, args, ctx) => {
        const { id } = args;
        return ctx.db.book_section.findOne({ where: { id: id }, select });
      }
    });
  }
});