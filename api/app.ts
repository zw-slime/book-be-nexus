import { use } from 'nexus';
import { prisma } from 'nexus-plugin-prisma';

use(prisma({ client: { options: { log: ['info', 'query'] } } }));